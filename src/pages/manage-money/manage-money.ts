import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ToastController } from 'ionic-angular';
import { AddEditMoneyPage } from '../add-edit-money/add-edit-money';
//https://ionicframework.com/docs/building/storage
import { Storage } from '@ionic/storage';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

@Component({
  selector: 'page-manage-money',
  templateUrl: 'manage-money.html',
})
export class ManageMoneyPage {
  pluswithkey = 'manageMoney';
  crMonth = '';
  items: Array<{key: string, item:any}>;
  totalOut: number = 0;
  totalIn: number = 0;
  diffInOut: number = 0;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toastController: ToastController,
    public alertController: AlertController,
    private storage: Storage) {
      let date = new Date();
      this.crMonth = date.getFullYear()
      + '-' + (((date.getMonth() +1 ) < 10 ) ? '0'+(date.getMonth()+1) : (date.getMonth()+1));
      //+ '-' + ((date.getDate() < 10) ? '0'+date.getDate() : date.getDate());

      this.loadDbData();
  }
  ionViewWillEnter() {
    this.calculateInOut();
  }
  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad ManageMoneyPage');
  // }

  editItem($event, item){
    this.navCtrl.push(AddEditMoneyPage, {
      item: item
    });
  }

  changedSearchDate($event){
    this.loadDbData();
  }

  deleteItem($event, item){
    this.deleteAlertConfirm(item);
  }

  //list word
  loadDbData(){
    this.items = [];
    this.totalOut = 0;
    this.totalIn = 0;
    this.diffInOut = 0;

    // get list datas
    this.storage.forEach( (value, key, index) => {
      //get data list
      if(key.indexOf(this.pluswithkey) > -1){
        let paydate = new Date(value.payDate);
        let searchDate = new Date(this.crMonth);

        if(paydate.getFullYear() === searchDate.getFullYear()
          && paydate.getMonth() === searchDate.getMonth()){
            this.items.push({
              key : key,
              item : value
            });

          // //typespending: 0 // 0: chi; 1: nhận
          // if(value.typespending == 0){
          //   this.totalOut = this.totalOut + Number(value.money);
          // }else if(value.typespending == 1){
          //   this.totalIn = this.totalIn +  Number(value.money);
          // }
        }
      }
    }).then(()=>{
      //this.diffInOut = this.totalIn - this.totalOut;
      this.calculateInOut();
    });
  }

  calculateInOut(){
    this.totalOut = 0;
    this.totalIn = 0;
    this.diffInOut = 0;
    if(this.items.length > 0){
      this.items.forEach((value, key, index) =>{
        //typespending: 0 // 0: chi; 1: nhận
        if(value.item.typespending == 0){
          this.totalOut = this.totalOut + Number(value.item.money);
        }else if(value.item.typespending == 1){
          this.totalIn = this.totalIn +  Number(value.item.money);
        }
      });
    }
    this.diffInOut = this.totalIn - this.totalOut;
  }

  // alert confirm
  async deleteAlertConfirm(item) {
    const alert = await this.alertController.create({
      title: 'Confirm!',
      message: 'Bạn muốn xóa?',
      buttons: [
        {
          text: 'Không',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Xóa!',
          handler: () => {
            // delete from storaage
            this.storage.remove(item.key).then(

              // delete item from screen list
              () => {
                var index = this.items.indexOf(item);
                if (index > -1) {
                  this.items.splice(index, 1);
                }
              }
            );
          }
        }
      ]
    });

    await alert.present();
  }
}
