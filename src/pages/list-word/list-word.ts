import { Component } from '@angular/core';
import { NavController, NavParams, AlertController,ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AddEditWordPage } from '../add-edit-word/add-edit-word';

@Component({
  selector: 'page-list-word',
  templateUrl: 'list-word.html',
})

export class ListWordPage {
  // collection data words
  dbData: Array<{key: string, word:any}>;
  items: Array<{key: string, word:any}>;
  isShowTheHideWords: boolean = false;

  exceptKeys = {
    pagesettings: "pagesettings"
  };

  // constructor
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    public toastController: ToastController,
    public alertController: AlertController) {
      // list words
      this.loadDbData();
  }

  // Event: editWord
  editWord(event, item) {
    this.navCtrl.push(AddEditWordPage, {
      item: item
    });
  }

  // delete a word
  deleteWord(event, item){
    this.presentAlertConfirm(item);
  }

  // go to Word page
  gotoWordPage(event){
    this.navCtrl.push(AddEditWordPage);
  }

  // ReloadList
  reloadList(event){
    // list words
    this.loadDbData();
  }

  // dbclick to hide item from the list
  // when this word was remembered
  hideFromList(event, item){
    // this word was remembered
    item.word.hideFromList = !(item.word.hideFromList);

    this.storage.set(item.key, item.word).then(
      ()=>{
        // remove session list
        if(item.word.hideFromList == true && this.isShowTheHideWords == false){

          var index = this.items.indexOf(item);
          if (index > -1) {
            this.items.splice(index, 1);
          }

          this.presentToast("Your words was hide!");
        }else{
          this.presentToast("Your words was back to list!");
        }
      }
    );
  }

  // list word
  loadDbData(){
    // list word
    this.dbData = [];
    this.items = [];
    // get list datas
    this.storage.forEach( (value, key, index) => {
      //get data list
      if(key.indexOf('|') == -1){
        this.dbData.push({
          key : key,
          word : value
        });
      }
    }).then(
      () =>{
        // sort desc
        this.dbData.reverse();

        // load setting
        var pagesettings = this.dbData.filter((item) => {
          return item.key == this.exceptKeys.pagesettings;
        });
        var settingsData = pagesettings.length > 0 ? pagesettings[0].word : null; 
        this.isShowTheHideWords = settingsData ? settingsData.isShowTheHideWords : false;

        // load list item
        this.items = this.dbData.filter((item) => {
          if(this.isShowTheHideWords)
            return item.key != this.exceptKeys.pagesettings;
          else if(!item.word.hideFromList)
            return item.key != this.exceptKeys.pagesettings;
          else
            return false;
        });

      }
    );
  }

  openSetting(event){
    this.settingsAlertConfirm();
  }

  // alert confirm
  async settingsAlertConfirm() {
    const alert = await this.alertController.create({
      title: 'Settings!',
     // message: 'Settings',
      inputs:[
        {
          type: 'checkbox',
          label: 'Show The Hide Words',
          value: 'showthehideword',
          checked: this.isShowTheHideWords
        }
      ],
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (data) => {

            let inputArray = <Array<object>>alert.data.inputs;

            inputArray.map((input) => {
              if(input['value'] == "showthehideword" && 
                this.isShowTheHideWords != input['checked']){

                  this.isShowTheHideWords = input['checked'];
                  // save setting to db
                  var settingsObject = {
                    isShowTheHideWords: this.isShowTheHideWords
                  };
                  this.storage.set("pagesettings", settingsObject).then(()=>{
                    this.loadDbData();
                  });

                }
             });

          }
        }
      ]
    });

    await alert.present();
  }

  // alert confirm
  async presentAlertConfirm(item) {
    const alert = await this.alertController.create({
      title: 'Confirm!',
      message: 'Are you sure you want to DELETE?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            // delete from storaage
            this.storage.remove(item.key).then(

              // delete item from screen list
              () => {
                var index = this.items.indexOf(item);
                if (index > -1) {
                  this.items.splice(index, 1);
                }
              }

            );
          }
        }
      ]
    });

    await alert.present();
  }

  // defined show Toast
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 2000
    });
    toast.present();
  }
}
