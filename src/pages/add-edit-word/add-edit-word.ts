import { Component, ViewChild} from '@angular/core';
import {ToastController} from 'ionic-angular';
import { NavParams } from 'ionic-angular';
//https://ionicframework.com/docs/building/storage
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-add-edit-word',
  templateUrl: 'add-edit-word.html',
})
export class AddEditWordPage {
  @ViewChild('inputKeyword') inputKeyword ;
  selectedWord: any;

  // object of form
  word = {
    keyword:'',
    description:''
  }

  // init
  constructor(
    private storage: Storage,
    public toastController: ToastController,
    public navParams: NavParams) {
      this.selectedWord = navParams.get('item');

      if(this.selectedWord){
        this.word = this.selectedWord.word;
      }
  }
  //set focus input text.
  //https://stackoverflow.com/questions/39612653/set-focus-on-an-input-with-ionic-2
  ionViewDidLoad() {
    setTimeout(() => {
      this.inputKeyword.setFocus();
    },1000);
  }

  // Event: add new word.
  addWord() {
    // validation is OK
    if(this.word.keyword){
      if(this.selectedWord){
        // Update
        this.updateWord();
      }else{
        // add new
        this.addNewWord();
      }
    }
    // validation is NG
    else{
      // show finished toast
      this.presentToast('Vui lòng nhập đầy đủ thông tin!');
    }
  }
 
  // Update
  updateWord(){
    this.storage.set(this.selectedWord.key, this.word).then(
      () =>{
        this.presentToast('Cập nhật thành công!');
        // set focus
        this.inputKeyword.setFocus();
      }
    );
  }
  
  // add a new word
  addNewWord(){
    var key = new Date().toISOString();
    this.storage.set(key, this.word).then(
      () =>{

        //reset object of form
        this.word = {
          keyword:'',
          description:''
        }
        // show finished toast
        this.presentToast('Thêm thành công!');
        // set focus
        this.inputKeyword.setFocus();
      }
    )
    .catch(function(error){
      // temporary: log error
      console.log(error);
      // set focus
      this.inputKeyword.setFocus();
    })
  }

  // defined show Toast
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 2000
    });
    toast.present();
  }
}
