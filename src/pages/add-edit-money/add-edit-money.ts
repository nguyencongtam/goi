import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, ToastController, NavParams } from 'ionic-angular';
//https://ionicframework.com/docs/building/storage
import { Storage } from '@ionic/storage';
/**
 * Generated class for the AddEditMoneyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-edit-money',
  templateUrl: 'add-edit-money.html',
})
export class AddEditMoneyPage {
  @ViewChild('inputKeyword') inputKeyword ;
  selectedItem: any; // item select from list
  pluswithkey = 'manageMoney';
  date = new Date();
  today:string;
  item :{
    money:number,
    description: string,
    payDate: string,
    typespending: number
  };

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toastController: ToastController,
    private storage: Storage) {
      // item selected from list
      this.selectedItem = navParams.get('item');
      if(this.selectedItem){
        this.item = this.selectedItem.item;
      }else{
        this.today = this.date.getFullYear()
          + '-' + (((this.date.getMonth() +1 ) < 10 ) ? '0'+ (this.date.getMonth() + 1) : (this.date.getMonth()+1))
          + '-' + ((this.date.getDate() < 10) ? '0' + this.date.getDate() : this.date.getDate());

        this.item = {
          money:0,
          description: '',
          payDate: this.today,
          typespending: 0 // 0: chi; 1: nhận
        };
      }
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad AddEditMoneyPage');
  // }
  //set focus input text.
  //https://stackoverflow.com/questions/39612653/set-focus-on-an-input-with-ionic-2
  ionViewDidLoad() {
    setTimeout(() => {
      this.inputKeyword.setFocus();
    },1000);
  }

  // add new 
  addItem(event){
    this.item.money =  Number(this.item.money);
    if(!this.item.money || this.item.money <= 0 || !this.item.money || !this.item.description){
      this.presentToast('Vui lòng nhập đầy đủ thông tin!');
    }else{
      if(this.selectedItem){
        this.updateItemDb();
      }else{
        this.addItemDb();
      }
    }
  }

  // Update
  updateItemDb(){
    this.storage.set(this.selectedItem.key, this.item).then(
      () =>{
        this.presentToast('Cập nhật thành công!');
        // set focus
        this.inputKeyword.setFocus();
      }
    );
  }

  addItemDb(){
    var key = new Date().toISOString() + '|' + this.pluswithkey;
    this.storage.set(key, this.item).then(
      () =>{
        //reset object of form
        this.item = {
          money:0,
          description: '',
          payDate: this.today,
          typespending: 0 // 0: chi; 1: nhận
        };
        // show finished toast
        this.presentToast('Thêm thành công!');
        // set focus
        this.inputKeyword.setFocus();
      }
    )
    .catch(function(error){
      // temporary: log error
      console.log(error);
      // set focus
      this.inputKeyword.setFocus();
    })
  }

  // defined show Toast
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 2000
    });
    toast.present();
  }
}
