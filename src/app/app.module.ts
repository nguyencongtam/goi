import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Chichai } from './app.component';

//import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { AddEditWordPage } from '../pages/add-edit-word/add-edit-word';
import { ListWordPage } from '../pages/list-word/list-word';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//https://ionicframework.com/docs/building/storage
import { IonicStorageModule } from '@ionic/storage';

import { OrderByPipe } from '../utilities/order-by-pipe';
import { ManageMoneyPage } from '../pages/manage-money/manage-money';
import { AddEditMoneyPage } from '../pages/add-edit-money/add-edit-money';

//https://ionicacademy.com/font-awesome-ionic-4/
//https://github.com/FortAwesome/angular-fontawesome
//https://fontawesome.com/icons?d=gallery&s=solid
//import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
//import { faCoffee } from '@fortawesome/free-solid-svg-icons';
//import { fas } from '@fortawesome/free-solid-svg-icons';
//import { far } from '@fortawesome/free-regular-svg-icons';
@NgModule({
  declarations: [
    Chichai,
    //HelloIonicPage,
    AddEditWordPage,
    ListWordPage,
    ItemDetailsPage,
    ListPage,
    ManageMoneyPage,
    AddEditMoneyPage,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    //FontAwesomeModule,
    IonicModule.forRoot(Chichai),
    IonicStorageModule.forRoot({
      name: '__goi_db',
      version: 1.0,
      size: 4980736,
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Chichai,
    //HelloIonicPage,
    AddEditWordPage,
    ListWordPage,
    ItemDetailsPage,
    ListPage,
    ManageMoneyPage,
    AddEditMoneyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    //library.add(faCoffee, fas);
  }
}
