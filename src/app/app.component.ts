import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

//import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { AddEditWordPage } from '../pages/add-edit-word/add-edit-word';
import { ListWordPage } from '../pages/list-word/list-word';
import { ManageMoneyPage } from '../pages/manage-money/manage-money';
import { AddEditMoneyPage } from '../pages/add-edit-money/add-edit-money';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})
export class Chichai {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  //rootPage = HelloIonicPage;
  rootPage = ListWordPage;
  pages: Array<{id:number, title: string, component: any}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      //{ title: 'Hello Ionic', component: HelloIonicPage },
      { id: 1, title: 'Từ vựng', component: AddEditWordPage },
      { id: 2, title: 'Danh sách từ vựng', component: ListWordPage },
      { id: 3, title: 'Chi tiêu', component: AddEditMoneyPage },
      { id: 3, title: 'Danh sách chi tiêu', component: ManageMoneyPage },
      //{ title: 'My First List', component: ListPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
